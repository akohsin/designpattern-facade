package barist;

import coffeeexpress.AdvancedCoffeeMachine;
import coffeeexpress.CoffeeBeverage;
import coffeeexpress.frother.MilkFrother;
import coffeeexpress.grinder.CoffeeBean;
import coffeeexpress.grinder.CoffeeGrinder;
import coffeeexpress.grinder.CoffeeGrinderException;
import coffeeexpress.ingredients.WaterIngredient;

public class Americano {
    AdvancedCoffeeMachine machine;
    CoffeeGrinder coffeeGrinder;


    public Americano(AdvancedCoffeeMachine machine, CoffeeGrinder coffeeGrinder) {
        this.machine = machine;
        this.coffeeGrinder = coffeeGrinder;
    }

    public CoffeeBeverage makeAmericano() {
        try {
            machine.addIngredients(new WaterIngredient(100));
            machine.addIngredients(coffeeGrinder.grind(new CoffeeBean()));

        } catch (CoffeeGrinderException cge) {
            System.out.println("Opróżniam młynek");
        } finally {
            return machine.getCoffee();
        }
    }
}
