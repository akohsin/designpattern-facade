package barist;

import coffeeexpress.AdvancedCoffeeMachine;
import coffeeexpress.CoffeeBeverage;
import coffeeexpress.frother.MilkFrother;
import coffeeexpress.frother.MilkFrotherException;
import coffeeexpress.frother.MilkIngredient;
import coffeeexpress.grinder.CoffeeBean;
import coffeeexpress.grinder.CoffeeGrinder;
import coffeeexpress.grinder.CoffeeGrinderException;
import coffeeexpress.ingredients.WaterIngredient;

public class Cappuccino {
    AdvancedCoffeeMachine machine;
    CoffeeGrinder coffeeGrinder;
    MilkFrother milkFrother;

    public Cappuccino(AdvancedCoffeeMachine machine, CoffeeGrinder coffeeGrinder, MilkFrother milkFrother) {
        this.machine = machine;
        this.coffeeGrinder = coffeeGrinder;
        this.milkFrother = milkFrother;
    }

    public CoffeeBeverage makeCappuccino() {
        try {
            machine.addIngredients(new WaterIngredient(30));
            machine.addIngredients(coffeeGrinder.grind(new CoffeeBean()));
            machine.addIngredients(milkFrother.froth(new MilkIngredient()));
          } catch (MilkFrotherException mfe) {
            System.out.println("błąd spieniacza");

        } catch (CoffeeGrinderException cge) {
            System.out.println("Opróżniam młynek");

        } finally {
            return machine.getCoffee();
        }
    }
}
