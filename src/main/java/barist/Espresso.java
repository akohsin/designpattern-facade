package barist;

import coffeeexpress.AdvancedCoffeeMachine;
import coffeeexpress.CoffeeBeverage;
import coffeeexpress.frother.MilkFrother;
import coffeeexpress.frother.MilkFrotherException;
import coffeeexpress.frother.MilkIngredient;
import coffeeexpress.grinder.CoffeeBean;
import coffeeexpress.grinder.CoffeeGrinder;
import coffeeexpress.grinder.CoffeeGrinderException;
import coffeeexpress.ingredients.WaterIngredient;

public class Espresso {
    AdvancedCoffeeMachine machine;
    CoffeeGrinder coffeeGrinder;


    public Espresso(AdvancedCoffeeMachine machine, CoffeeGrinder coffeeGrinder) {
        this.machine = machine;
        this.coffeeGrinder = coffeeGrinder;

    }

    public CoffeeBeverage makeEspresso() {
        try {
            machine.addIngredients(new WaterIngredient(30));
            machine.addIngredients(coffeeGrinder.grind(new CoffeeBean()));
        } catch (CoffeeGrinderException cge) {
            System.out.println("Opróżniam młynek");
        } finally {
            return machine.getCoffee();
        }
    }
}
