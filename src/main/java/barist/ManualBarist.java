package barist;

import coffeeexpress.CoffeeBeverage;
import coffeeexpress.AdvancedCoffeeMachine;
import coffeeexpress.frother.MilkFrother;
import coffeeexpress.frother.MilkFrotherException;
import coffeeexpress.frother.MilkIngredient;
import coffeeexpress.grinder.CoffeeBean;
import coffeeexpress.grinder.CoffeeGrinder;
import coffeeexpress.grinder.CoffeeGrinderException;
import coffeeexpress.ingredients.SugarIngredient;
import coffeeexpress.ingredients.WaterIngredient;

public class ManualBarist {
    public static void main(String[] args) {
        AdvancedCoffeeMachine machine = new AdvancedCoffeeMachine();
        CoffeeGrinder coffeeGrinder = new CoffeeGrinder();
        MilkFrother milkFrother = new MilkFrother();
        try {
            System.out.println("przygotowuje cappucino");
            machine.addIngredients(new WaterIngredient(100));
            machine.addIngredients(coffeeGrinder.grind(new CoffeeBean()));
            machine.addIngredients(milkFrother.froth(new MilkIngredient()));
            System.out.println("zrobiona kawa to: " + machine.getCoffee().toString());
        } catch (MilkFrotherException mfe) {
            System.out.println("błąd spieniacza");
        } catch (CoffeeGrinderException cge) {
            System.out.println("opróżniam pojemnik na kawe, rozpocznij robienie kawy od nowa");
            coffeeGrinder.empty();
        } finally {

        }
        try {
            System.out.println("przygotowuje latte");
            machine.addIngredients(new WaterIngredient(100));
            machine.addIngredients(coffeeGrinder.grind(new CoffeeBean()));
            machine.addIngredients(milkFrother.froth(new MilkIngredient()));
            machine.addIngredients(new SugarIngredient());
            System.out.println("zrobiona kawa to: " + machine.getCoffee().toString());
        } catch (MilkFrotherException mfe) {
            System.out.println("błąd spieniacza, rozpocznij robienie kawy od nowa");

        } catch (CoffeeGrinderException cge) {
            System.out.println("opróżniam pojemnik na kawe, rozpocznij robienie kawy od nowa");
            coffeeGrinder.empty();
        } finally {
        }
        try {
            System.out.println("przygotowuje cappucino");
            machine.addIngredients(new WaterIngredient(30));
            machine.addIngredients(coffeeGrinder.grind(new CoffeeBean()));
            machine.addIngredients(milkFrother.froth(new MilkIngredient()));
            System.out.println("cukier? niee");
            System.out.println("zrobiona kawa to: " + machine.getCoffee().toString());
        } catch (MilkFrotherException mfe) {
            System.out.println("błąd spieniacza, rozpocznij robienie kawy od nowa");

        } catch (CoffeeGrinderException cge) {
            System.out.println("opróżniam pojemnik na kawe, rozpocznij robienie kawy od nowa");
            coffeeGrinder.empty();
        } finally {
        }
        try {
            System.out.println("przygotowuje americano");
            machine.addIngredients(new WaterIngredient(100));
            machine.addIngredients(coffeeGrinder.grind(new CoffeeBean()));
            System.out.println("zrobiona kawa to: " + machine.getCoffee().toString());
        } catch (CoffeeGrinderException cge) {
            System.out.println("opróżniam pojemnik na kawe, rozpocznij robienie kawy od nowa");
            coffeeGrinder.empty();
        } finally {
        }
        try {
            System.out.println("przygotowuje cappucino");
            machine.addIngredients(new WaterIngredient(30));
            machine.addIngredients(coffeeGrinder.grind(new CoffeeBean()));
            machine.addIngredients(milkFrother.froth(new MilkIngredient()));
            System.out.println("cukier? niee");
            System.out.println("zrobiona kawa to: " + machine.getCoffee().toString());
        } catch (MilkFrotherException mfe) {
            System.out.println("błąd spieniacza, rozpocznij robienie kawy od nowa");

        } catch (CoffeeGrinderException cge) {
            System.out.println("opróżniam pojemnik na kawe, rozpocznij robienie kawy od nowa");
            coffeeGrinder.empty();
        } finally {

        }
    }
}
