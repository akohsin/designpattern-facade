package facade;

import barist.Americano;
import barist.Cappuccino;
import barist.Espresso;
import barist.Latte;
import coffeeexpress.AdvancedCoffeeMachine;
import coffeeexpress.frother.MilkFrother;
import coffeeexpress.grinder.CoffeeGrinder;

public class SimpleCoffeeMachine {
    AdvancedCoffeeMachine machine = new AdvancedCoffeeMachine();
    CoffeeGrinder coffeeGrinder = new CoffeeGrinder();
    MilkFrother milkFrother = new MilkFrother();
    Americano americano = new Americano(machine, coffeeGrinder);
    Cappuccino cappuccino = new Cappuccino(machine, coffeeGrinder, milkFrother);
    Espresso espresso = new Espresso(machine, coffeeGrinder);
    Latte latte = new Latte(machine, coffeeGrinder, milkFrother);

    public String makeAmericano() {
        return americano.makeAmericano().toString();
    }

    public String makeCappucino() {
        return cappuccino.makeCappuccino().toString();
    }

    public String makeEspresso() {
        return espresso.makeEspresso().toString();
    }

    public String makeLatte() {
        return latte.makeLatte().toString();
    }

    public static void main(String[] args) {
        SimpleCoffeeMachine facade = new SimpleCoffeeMachine();
        System.out.println(facade.makeAmericano());
        System.out.println(facade.makeCappucino());
        System.out.println(facade.makeLatte());
        System.out.println(facade.makeEspresso());
        System.out.println(facade.makeLatte());
        System.out.println(facade.makeEspresso());
        System.out.println(facade.makeLatte());
        System.out.println(facade.makeEspresso());
        System.out.println(facade.makeEspresso());
        System.out.println(facade.makeEspresso());
    }


}
