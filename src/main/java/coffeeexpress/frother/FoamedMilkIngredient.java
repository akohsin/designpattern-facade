package coffeeexpress.frother;

import coffeeexpress.CoffeeIngredient;

public class FoamedMilkIngredient extends CoffeeIngredient {
    FoamedMilkIngredient() {
    }
    public String toString(){
        return "Foamed Milk";
    }
}
