package coffeeexpress.frother;

public class MilkFrotherException extends Exception {
    public MilkFrotherException(String message) {
        super(message);
    }
}
