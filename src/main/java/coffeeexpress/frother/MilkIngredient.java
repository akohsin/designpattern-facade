package coffeeexpress.frother;

import coffeeexpress.CoffeeIngredient;

public class MilkIngredient extends CoffeeIngredient {
    public String toString(){
        return "Milk";
    }

}
