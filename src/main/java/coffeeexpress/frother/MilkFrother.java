package coffeeexpress.frother;

import java.util.Random;

public class MilkFrother {
    Random rand = new Random();

    public FoamedMilkIngredient froth(MilkIngredient milk) throws MilkFrotherException{
        if (rand.nextInt(11)/10>0.8){
            throw new MilkFrotherException("Błąd spieniacza");
        }
        else return new FoamedMilkIngredient();
    }
}
