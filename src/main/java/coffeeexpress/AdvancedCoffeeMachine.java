package coffeeexpress;

import coffeeexpress.CoffeeBeverage;
import coffeeexpress.CoffeeIngredient;

public class AdvancedCoffeeMachine {
    CoffeeBeverage coffee = new CoffeeBeverage();
    public void addIngredients(CoffeeIngredient ingredient){
        coffee.addIngredients(ingredient);
    }

    public CoffeeBeverage getCoffee() {
        CoffeeBeverage tmp = coffee;
        coffee= new CoffeeBeverage();
        return tmp;
    }
}
