package coffeeexpress.grinder;

import coffeeexpress.CoffeeIngredient;

public class CoffeePowderIngredient extends CoffeeIngredient {

    public String toString(){
        return "Coffee Powder";
    }
}
