package coffeeexpress.grinder;

public class CoffeeGrinder {
    public int wasteContainerCount = 0;

    public void empty() {
        wasteContainerCount = 0;
    }

    public CoffeePowderIngredient grind(CoffeeBean bean) throws CoffeeGrinderException {

        if (wasteContainerCount > 3) {
            empty();
            throw new CoffeeGrinderException("full waste container");
        } else {
            wasteContainerCount++;
            return new CoffeePowderIngredient();
        }
    }
}


