package coffeeexpress.grinder;

public class CoffeeGrinderException extends Exception {
    public CoffeeGrinderException(String message) {
        super(message);
    }
}
