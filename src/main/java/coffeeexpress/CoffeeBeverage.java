package coffeeexpress;

import java.util.ArrayList;
import java.util.List;

public class CoffeeBeverage {
    private List<CoffeeIngredient> listOfIngredients = new ArrayList<CoffeeIngredient>();

    @Override
    public String toString() {
        String tmp="";
        for (CoffeeIngredient ing : listOfIngredients) {
            tmp += " " + ing.toString();

        }
        return "CoffeeBeverage {" +
                "listOfIngredients =" + tmp +
                '}';
    }

    public void addIngredients(CoffeeIngredient ingredient) {
        listOfIngredients.add(ingredient);
    }
}
