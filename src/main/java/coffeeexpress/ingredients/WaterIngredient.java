package coffeeexpress.ingredients;

import coffeeexpress.CoffeeIngredient;

public class WaterIngredient extends CoffeeIngredient {
    int amount;

    public WaterIngredient(int amount) {
        this.amount = amount;
    }
    public String toString(){
        return "Water: "+ amount;
    }
}
