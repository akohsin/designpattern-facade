package coffeeexpress.ingredients;

import coffeeexpress.CoffeeIngredient;

public class SugarIngredient extends CoffeeIngredient {
    public String toString(){
        return "Sugar";
    }

}
